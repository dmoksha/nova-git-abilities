const gitBlame = require('git_blame')
const fileCommands = require('open_copy_links')

// Invoked by the "showGitBlame" command
nova.commands.register("nova-git-abilities.showGitBlame", (editor) => {
  gitBlame(editor);
});

// Invoked by the "openFileInBrowser" command
nova.commands.register("nova-git-abilities.openFileInBrowser", (editor) => {
  fileCommands.openFileInBrowser(editor);
});

// Invoked by the "openSelectionInBrowser" command
nova.commands.register("nova-git-abilities.openSelectionInBrowser", (editor) => {
  fileCommands.openFileInBrowser(editor, true);
});

// Invoked by the "openSelectionInBrowserDefaultBranch" command
nova.commands.register("nova-git-abilities.openSelectionInBrowserDefaultBranch", (editor) => {
  fileCommands.openFileInBrowser(editor, true, true);
});

// Invoked by the "copyFileLink" command
nova.commands.register("nova-git-abilities.copyFileLink", (editor) => {
  fileCommands.copyFileLink(editor);
});

// Invoked by the "copySelectionLink" command
nova.commands.register("nova-git-abilities.copySelectionLink", (editor) => {
  fileCommands.copyFileLink(editor, true);
});

// Invoked by the "copySelectionLinkDefaultBranch" command
nova.commands.register("nova-git-abilities.copySelectionLinkDefaultBranch", (editor) => {
  fileCommands.copyFileLink(editor, true, true);
});

exports.activate = function() {
  // Do work when the extension is activated
  console.log("activate");
}

exports.deactivate = function() {
  // Clean up state before the extension is deactivated
  console.log("deactivate");
}
