const processUtils = require('process_utils')

exports.getBlame = getBlame
exports.getRepoUri = getRepoUri
exports.getCommitRef = getCommitRef
exports.getDefaultBranchName = getDefaultBranchName

async function getBlame(document, startLine, endLine) {
  const processOptions = {
    cwd: nova.path.dirname(document.path),
    args: [
      'git',
      'blame',
      '--line-porcelain',
      '--date',
      'local',
      '-L',
      `${startLine},${endLine}`,
      document.path,
    ]
  }

  const output = await processUtils.getProcessOutput(processOptions).catch((failure) => console.log(failure.stderr.trim()))
  const lines = output.stdoutLines
  const blame = lines.reduce((previousValue, line, currentIndex, array) => {
    const parts = line.split(' ')
    const key = parts.shift()

    if (key) {
      previousValue[key] = parts.join(' ').replace(document.eol, '')
    }

    return previousValue
  }, {})

  return blame
}

async function getRepoUri(document) {
  console.log('getRepoUri')
  const processOptions = {
    cwd: nova.path.dirname(document.path),
    args: ['git', 'remote', 'get-url', 'origin']
  }

  const gitRemoteOutput = await processUtils.getProcessOutput(processOptions).catch((failure) => console.log(failure.stderr.trim()))
  var fetchUrl = gitRemoteOutput.stdoutLines[0].replace(document.eol, '')
  console.log(`Remote: ${fetchUrl}`)

  const repoUri = buildUrl(fetchUrl)
  console.log(`Repository: ${repoUri}`)
  // test_parseURL()

  return repoUri
}

async function getCommitRef(document) {
  const processOptions = {
    cwd: nova.path.dirname(document.path),
    args: ['git', 'rev-parse', 'HEAD']
  }

  const commitOutput = await processUtils.getProcessOutput(processOptions).catch((failure) => console.log(failure.stderr.trim()))
  commit = commitOutput.stdoutLines[0].replace(document.eol, '')
  console.log(`Commit: ${commit}`)

  return commit
}

async function getDefaultBranchName(document) {
  const processOptions = {
    cwd: nova.path.dirname(document.path),
    args: ['git', 'branch', '-r']
  }

  const branchOutput = await processUtils.getProcessOutput(processOptions).catch((failure) => console.log(failure.stderr.trim()))
  const branchList = branchOutput.stdoutLines.join("\n")
  const matches = branchList.match(/origin\/HEAD -> origin\/(.*)/)
  const defaultBranch = matches?.[1]

  console.log(`defaultBranch: ${defaultBranch}`)
  console.log(`branch list: ${branchList}`)

  return defaultBranch
}

function buildUrl(remoteUri) {
  if (!(remoteUri.startsWith('https:') || remoteUri.startsWith('http:'))) {
    // ssh style uri such as git@gitlab.com:dmoksha/nova-extensions/nova-git-abilities
    // convert to normal http uri
    remoteUri = remoteUri.replace(':', '/')
    remoteUri = 'https://' + remoteUri
  }

  const parsed = parseUrl(remoteUri)

  // if port was specified for web url, let's keep it and use later
  var port
  if (parsed.protocol.startsWith('http')) { port = parsed.port }

  // if it's not http, force to https
  if (parsed.protocol != 'http:') { parsed.protocol = 'https:' }

  fetchUrl = parsed.protocol + '//' + parsed.hostname
  if (port) { fetchUrl += ':' + port }
  fetchUrl += parsed.pathname

  console.log(`Parsed: ${fetchUrl}`)

  // GitLab has .git appended
  const repoUrl = fetchUrl.replace(/.git$/, '')

  // remove the user if it's still there - https://git@something
  const repoUrlNoUser = repoUrl.replace(/:\/\/.*@/, '://')

  return repoUrlNoUser
}

// pulled from https://stackoverflow.com/a/39308026/2781043
function parseUrl(url) {
    var m = url.match(/^((?:([^:\/?#]+:)(?:\/\/))?((?:([^\/?#:]*)(?::([^\/?#:]*))?@)?([^\/?#:]*)(?::([^\/?#:]*))?))?([^?#]*)(\?[^#]*)?(#.*)?$/),
        r = {
            hash: m[10] || "",                   // #asd
            host: m[3] || "",                    // localhost:257
            hostname: m[6] || "",                // localhost
            href: m[0] || "",                    // http://username:password@localhost:257/deploy/?asd=asd#asd
            origin: m[1] || "",                  // http://username:password@localhost:257
            pathname: m[8] || (m[1] ? "/" : ""), // /deploy/
            port: m[7] || "",                    // 257
            protocol: m[2] || "",                // http:
            search: m[9] || "",                  // ?asd=asd
            username: m[4] || "",                // username
            password: m[5] || ""                 // password
        }
    if (r.protocol.length == 2) {
        r.protocol = "file:///" + r.protocol.toUpperCase()
        r.origin = r.protocol + "//" + r.host
    }
    r.href = r.origin + r.pathname + r.search + r.hash
    return r
}

function test_parseURL() {
  const urlMatrix = [
    ['https://gitlab.com/dmoksha/nova-extensions/nova-git-abilities.git', 'https://gitlab.com/dmoksha/nova-extensions/nova-git-abilities'],
    ['https://git@mylocal.com:5022/dmoksha/nova-extensions/nova-git-abilities.git', 'https://mylocal.com:5022/dmoksha/nova-extensions/nova-git-abilities'],
    ['git@gitlab.com:dmoksha/nova-extensions/nova-git-abilities', 'https://gitlab.com/dmoksha/nova-extensions/nova-git-abilities'],
    ['git+ssh://git@mylocal.com:5022/dmoksha/nova-extensions/nova-git-abilities', 'https://mylocal.com:5022/dmoksha/nova-extensions/nova-git-abilities'],
    ['git@gitlab.com:dmoksha/nova-extensions/nova-git-abilities.git', 'https://gitlab.com/dmoksha/nova-extensions/nova-git-abilities'],
  ]

  urlMatrix.forEach(function (item, index) {
    url = buildUrl(item[0])
    final_url = item[1]

    if (url != final_url) console.error(`Failed match!!!!: ${url} instead of ${final_url}`)
  });
}
