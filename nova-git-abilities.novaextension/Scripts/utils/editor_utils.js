exports.startEndRange = startEndRange;

function startEndRange(editor) {
  const lineRange = editor.getLineRangeForRange(editor.selectedRange)
  const startLine =
    (editor.getTextInRange(new Range(0, lineRange.start)).match(new RegExp(editor.document.eol, 'g'))
      ?.length ?? 0) + 1
  const endLine = editor
    .getTextInRange(new Range(0, lineRange.end))
    .match(new RegExp(editor.document.eol, 'g'))?.length

  return { startLine, endLine }
}
