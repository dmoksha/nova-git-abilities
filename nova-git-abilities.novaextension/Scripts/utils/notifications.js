const REQUEST_TIMEOUT = 3000

function sendRequest(notificationRequest) {
  setTimeout(cancelRequest.bind(null, notificationRequest), REQUEST_TIMEOUT)

  return nova.notifications.add(notificationRequest)
}

function cancelRequest(notificationRequest) {
  nova.notifications.cancel(notificationRequest.identifier)
}

exports.sendRequest = sendRequest;
exports.cancelRequest = cancelRequest;
