const processUtils = require('./utils/process_utils')
const notifications = require('./utils/notifications')
const editorUtils = require('./utils/editor_utils')
const gitUtils = require('./utils/git_utils')

module.exports = gitBlame;

async function gitBlame(editor) {
  console.log('gitBlame');

  const document = editor.document;
  const { startLine, endLine } = editorUtils.startEndRange(editor)

  const blame = await gitUtils.getBlame(document, startLine, endLine)
  const repoUri = await gitUtils.getRepoUri(document)
  const commit = await gitUtils.getCommitRef(document)

  const notification = new NotificationRequest(`git-blame-${startLine}-${endLine}`)
  const date = new Date(Number(blame['author-time']) * 1000)
  const message = `${blame['author-mail']}\n${date.toLocaleString()}\n\n${blame.summary}`

  notification.title = blame.author
  notification.body = message

  const hostName = url => /^(?:[^:]+:\/\/(?:[^@\/?]+@)?([^:\/?]+))?/.exec(url)[1]
  notification.actions = [`View on ${hostName(repoUri)}`]

  notifications.sendRequest(notification)
    .then(notificationResponse => {
      if (notificationResponse.actionIdx === 0) {
        if (repoUri && commit) {
          const blameUrl = `${repoUri}/blame/${commit}/${blame.filename}#L${startLine}-L${endLine}`
          console.log(`Opening: ${blameUrl}`)

          nova.openURL(blameUrl)
        } else {
          console.error('Could not open repository')
        }
      }
    })
};
