const gitUtils = require('./utils/git_utils')
const editorUtils = require('./utils/editor_utils')

exports.openFileInBrowser = openFileInBrowser;
exports.copyFileLink = copyFileLink;

async function openFileInBrowser(editor, includeLines = false, useDefaultBranch = false) {
  const repoUri = await gitUtils.getRepoUri(editor.document)

  var commit
  if (useDefaultBranch) {
    commit = await gitUtils.getDefaultBranchName(editor.document)
  } else {
    commit = await gitUtils.getCommitRef(editor.document)
  }

  const gitRelativePath = nova.workspace.relativizePath(editor.document.path);
  const { startLine, endLine } = editorUtils.startEndRange(editor)
  const lineRange = (includeLines) ? `#L${startLine}-L${endLine}` : ''
  const targetUrl = `${repoUri}/blob/${commit}/${gitRelativePath}${lineRange}`

  console.log(`Opening: ${targetUrl}`)

  nova.openURL(targetUrl)
};

async function copyFileLink(editor, includeLines = false, useDefaultBranch = false) {
  const repoUri = await gitUtils.getRepoUri(editor.document)

  var commit
  if (useDefaultBranch) {
    commit = await gitUtils.getDefaultBranchName(editor.document)
  } else {
    commit = await gitUtils.getCommitRef(editor.document)
  }

  const gitRelativePath = nova.workspace.relativizePath(editor.document.path);
  const { startLine, endLine } = editorUtils.startEndRange(editor)
  const lineRange = (includeLines) ? `#L${startLine}-L${endLine}` : ''
  const targetUrl = `${repoUri}/blob/${commit}/${gitRelativePath}${lineRange}`


  console.log(`Copying link: ${targetUrl}`)

  nova.clipboard.writeText(targetUrl)
};
