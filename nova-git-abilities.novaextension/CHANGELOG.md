### Version 1.1.5

- More fixes on remote uri parsing

### Version 1.1.4

- Added more robust parsing of remote uri

### Version 1.1.3

- Correctly parse ssh style remote uri

### Version 1.1.2

- Fix description and README

### Version 1.1

- added the following abilities
  - `Open Selection in Browser`: The current file is opened in the browser, highlighting
    the line(s) that were selected.
  - `Open Selection in Browser (origin/HEAD)`: The current file is opened in the browser,
    at the default branch (usually `master` or `main`), highlighting the line(s) that were selected.
  - `Copy Link to Selection`: A link to the current file is copied to the clipboard
  - `Open Selection in Browser (origin/HEAD)`: A link to the current file,
    at the default branch (usually `master` or `main`), is copied to the clipboard.
- fixed an issue parsing GitHub repository origins

### Version 1.0

- Initial release
- added the following ability
  - `Git blame`: The selected lines in the editor will be passed to `git blame`,
    and a notification will be shown with the author's name and email, and the commit's
    date and message. A button is included which opens the blame on GitHub, GitLab, or
    whichever host your repository uses.
