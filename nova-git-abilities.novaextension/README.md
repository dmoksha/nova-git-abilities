**Git Abilities** provides

- `Git blame`: The selected lines in the editor will be passed to `git blame`,
  and a notification will be shown with the author's name and email, and the commit's
  date and message. A button is included which opens the blame on GitHub, GitLab, or
  whichever host your repository uses.
- `Open Selection in Browser`: The current file is opened in the browser, highlighting
  the line(s) that were selected.
- `Open Selection in Browser (origin/HEAD)`: The current file is opened in the browser,
  at the default branch (usually `master` or `main`), highlighting the line(s) that were selected.
- `Copy Link to Selection`: A link to the current file is copied to the clipboard
- `Open Selection in Browser (origin/HEAD)`: A link to the current file,
  at the default branch (usually `master` or `main`), is copied to the clipboard.

The extension supports both [GitLab](https://gitlab.com) and [GitHub](https://github.com).

## Usage

Use one of the following to run Git Abilities:

- Select the **Editor → Git Abilities** menu item
- Choose **Git Abilities** from the context menu
- Open the command palette and type one of the commands listed above

Note: if you have commits that have **not** been pushed to the remote
repository, the file can not be opened in the browser since the commit
will not exist yet.  Instead you can use the `(origin/HEAD)` version of the
command

---

#### Acknowledgements

Built on the excellent work from [alexanderflink/nova-git-blame](https://github.com/alexanderflink/nova-git-blame) and [ticky/nova-github-shortcuts](https://github.com/ticky/nova-github-shortcuts)
